import mongoose from "mongoose"
import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"

import mess from "../config/mess"
const saltRound = 10

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, mess.REQUIRED]
    },
    email: {
        type: String,
        lowercase: true,
        unique: true,
        required: [true, mess.REQUIRED],
        index: true
    },
    password: {
        type: String,
        required: [true, mess.REQUIRED]
    }
})


UserSchema.methods.setPassword = function (password) {
    this.password = bcrypt.hashSync(password, saltRound);
}

UserSchema.methods.validatePassword = function (password) {
    return bcrypt.compareSync(password, this.password);
}

UserSchema.methods.generateJWT = function () {
    return jwt.sign(
        {
            email: this.email,
            id: this._id
        },
        "secret"
    )
}

UserSchema.methods.toAuthJson = function () {
    return {
        _id: this._id,
        email: this.email,
        token: this.generateJWT()
    }
}
module.exports = mongoose.model("User", UserSchema)