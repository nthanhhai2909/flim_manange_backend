import mongoose from "mongoose"
import mess from "../config/mess"


const manufacturer_enum = ["VTV", "HTV", "HBO", "StartMovie", "VTC"]

const FlimSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, mess.REQUIRED]
    },
    img: {
        type: String,
        required: [true, mess.REQUIRED]
    },
    director: {
        type: String
    },
    manufacturer: {
        type: String,
        enum: manufacturer_enum
    },
    date_start: {
        type: Date,
        validate: function(e) {
            if (!!e) {
                return e.getTime() < new Date()
            }
        }
    },
    date_release: {
        type: Date,
        validate: function(e) {
            if (!!e) {
                return  e.getTime() > new Date()
            }
        }
    }
})

FlimSchema.methods.getManufacturer = function() {
    return manufacturer_enum;
}

module.exports = mongoose.model("Flim", FlimSchema)