"use strict"
import express from "express"
import fs from "fs"
import FlimModel from "../../models/flim"
import mess from "../../config/mess"
import auth from "../../utils/auth"
import upload from "../../utils/multer"
import cloudinary from "../../utils/cloudinary_upload"

const LIMIT = 10
const router = express.Router()

router.get("/manufacturers", (req, res, next) => {
    return res.status(200).json({ manufacturers: new FlimModel().getManufacturer() })
})

// page is null => return all flims
// page is not null => return pagination
router.get("/flims", async (req, res, next) => {
    const page = req.query.page
    let count = await FlimModel.count({})
    let totalPage = parseInt(((count - 1) / LIMIT) + 1)

    if (!page) {
        FlimModel.find({}, (err, docs) => {
            if (err) { return res.status(500).json(err) }
            return res.status(200).json({ flims: docs, totalPage })
        })
        return
    }

    if ((parseInt(page) < 1) || (parseInt(page) > totalPage)) {
        return res.status(200).json({ data: [], totalPage })
    }

    FlimModel
        .find({})
        .skip(LIMIT * (parseInt(page) - 1))
        .limit(LIMIT)
        .exec((err, docs) => {
            if (err) { return res.status(500).json({ msg: err }) }
            return res.status(200).json({ data: docs, totalPage })
        })

})

router.post("/add", auth.authentication, upload.single("img"), async (req, res, next) => {
    const flim = req.body

    if (!flim.name || !req.file) {
        return res.status(422).json({ message: mess.INVALID_DATA })
    }

    // upload img to cloudinary
    let url = await cloudinary.upload_img(req.file.path)
    deleteFileAtPath(req.file.path)

    if (url == false) {
        return res.status(500).json({ message: mess.UPLOAD_IMGAGE_FALSE })
    }

    let finalFlim = new FlimModel({
        name: flim.name,
        img: url,
        manufacturer: flim.manufacturer
    })

    if (!!flim.date_start) {
        finalFlim.date_start = flim.date_start
    }

    if (!!flim.date_release) {
        finalFlim.date_release = flim.date_release
    }

    finalFlim.save((err, docs) => {
        if (err) {
            console.log(err)
            res.status(400).json({ message: err })
        }
        res.status(202).end();
    })
})


router.post("/update", auth.authentication, upload.single("img"), async (req, res, next) => {
    const flim = req.body

    if (!flim._id) {
        return res.status(422).json({ message: mess.INVALID_DATA })
    }

    let flimFind
    try {
        flimFind = await FlimModel.findById(flim._id)
    } catch (err) {
        return res.status(400).json({ msg: err })
    }

    if (flimFind === null) {
        return res.status(404).json({ msg: "Not found" })
    }

    if (req.file) {
        let url = await cloudinary.upload_img(req.file.path)
        deleteFileAtPath(req.file.path)
        if (url == false) {
            return res.status(500).json({ message: mess.UPLOAD_IMGAGE_FALSE })
        }
        flimFind.img = url
    }
    flimFind.name = !!flim.name ? flim.name : flimFind.name
    flimFind.director = flim.director 
    flimFind.manufacturer = !!flim.manufacturer  ? flim.manufacturer : flimFind.manufacturer
    flimFind.date_start =  flim.date_start 
    flimFind.date_release = flim.date_release 
    flimFind.save((err, docs) => {
        if (err) { res.status(400).json({ message: err }) }
        res.status(200).end();
    })
})


router.get("/delete", auth.authentication, async (req, res, next) => {
    const _id = req.query.id
    let flimFind;
    try {
        flimFind = await FlimModel.findById(_id)
    }
    catch (err) {
        return res.status(500).json({ message: err })
    }
    flimFind.remove()
    res.status(200).end()
})

const deleteFileAtPath = (path) => {
    fs.unlink(path, (err) => {
        if (err) { }
        console.log('path/file.txt was deleted');
    })
}
module.exports = router