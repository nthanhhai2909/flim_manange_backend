"use strict"
import express from "express"
import validation from "../../validation/validation"
import mess from "../../config/mess"
import UserModel from "../../models/user"

const router = express.Router()

router.post("/signup", async (req, res, next) => {
    const user = req.body
    // validate mandatory form data

    if (!user.name || !user.email || !user.password || !validation.validationEmail(user.email) || !validation.validationPassword(user.password)) {
        return res.status(422).json({message: mess.INVALID_DATA})
    }

    let userFind
    try {
        userFind = await UserModel.findOne({ email: user.email })
    } catch (err) {
        return res.status(500).json({errors: err})
    }

    if (userFind != null) {
        return res.status(409).json({message: mess.EMAIL_ALREADY})
    }

    let finalUser = new UserModel(user)
    finalUser.setPassword(user.password)
    finalUser.save((err, docs) => {
        if (err) {return res.status(400).json(err)}
        res.status(202).json({message: mess.ACCOUNT_HAS_BEEN_CREATED })
    })
})

router.post("/login", async (req, res, next) => {
    const user = req.body

    // validate mandatory form data
    if (!user.email || !user.password) {
        return res.status(422).json({message: mess.INVALID_DATA})
    }

    let userFind
    try {
        userFind = await UserModel.findOne({ email: user.email })
    } catch (err) {
        return res.status(500).json({message: err})
    }

    if (userFind == null) {
        return res.status(404).json({message: mess.EMAIL_OR_PASSWORD_INCORRECT})
    }

    if (userFind.validatePassword(user.password)) {
        return res.status(200).json(userFind.toAuthJson())
    } else {
        return res.status(403).json({
            message: mess.EMAIL_OR_PASSWORD_INCORRECT
        })
    }
})


module.exports = router


