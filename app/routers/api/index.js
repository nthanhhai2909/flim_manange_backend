import express from "express"
import user from "./user"
import flim from "./flim"
import auth from "../../utils/auth"
const router = express.Router()

router.use("/user", user)
router.use("/flim", flim)
router.get("/auth",  auth.authentication, (req, res) => {res.status(200).json({message: "token valid"})})



module.exports = router