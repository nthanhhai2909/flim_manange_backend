import jwt from "jsonwebtoken"
import UserModel from "../models/user"
import Mess from "../config/mess"
exports.authentication = (req, res, next) => {
    let result
    try {
        result = jwt.verify(req.headers.authorization, "secret")
    } catch (err) {
        return res.status(403).json({
            message: Mess.TOKEN_INVALID
        })
    }
    UserModel.findById(result.id, (err, docs) => {
        if (err) {
            return res.status(403).json({ message: err })
        }
        next()
    })
}
