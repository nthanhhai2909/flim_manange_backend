import express from "express"
import bodyParser from "body-parser"
import mongoose from "mongoose"
import path from 'path'
import cors from "cors"
import routers from "./app/routers"
import db from "./app/config/db"

const app = express();
app.use(cors())
const port = process.env.PORT || 8080

//Configure mongoose's promise to global promise
mongoose.promise = global.Promise
mongoose.connect(db.local_db)
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));
app.use(routers)

const server = app.listen(port, function() {
  console.log("Listening on port " + server.address().port)
})

